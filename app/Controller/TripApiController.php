<?php
App::uses('AppController', 'Controller');
App::uses('HTTPHelper', 'Lib');
class TripApiController extends AppController {

	public $uses = ['Place'];

	public function getPlaceDetails() {
		$this->autoRender = false;
		$placeid = $this->params['id'];
		$request = new HTTPHelper();

		$request = new HTTPHelper();
		// cek di db
		$data = $this->Place->findByPlaceId($placeid);
		if (empty($data)) {
			// cari di google
			$data = json_decode($request->request_data("https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $placeid . "&key=AIzaSyBrE8NmfqkCIrpy_Lu26JiqAaF1w7oUY0I"), true);
			
			if (!empty($data)){
				
				$result['address'] = $data['result']['vicinity'];
				$result['rating'] = $data['result']['rating'];
				$result['place_id'] = $data['result']['place_id'];
				$result['website'] = $data['result']['website'];
				$result['id'] = $data['result']['id'];
				$result['phone'] = $data['result']['formatted_phone_number'];
				$result['name'] = $data['result']['name'];
				$result['latitude'] = $data['result']['geometry']['location']['lat'];
				$result['longitude'] = $data['result']['geometry']['location']['lng'];
				$opening = '-';
				if (isset($data['result']['opening_hours'])) {
					$opening = '';
					foreach ($data['result']['opening_hours']['weekday_text'] as $key => $value) {
						$opening .= $value . "\n";
					}
				}
				$result['opening_hours'] = $opening;

				foreach ($data['result']['photos'] as $key => $value) {
					$result['photos'][] = $request->get_photos($value['photo_reference']);
				}
			//if (!empty($result['photos'][0])) {
					$result['singlephoto'] = $result['photos'][0];
				//}


				$this->Place->create();
				$place['Place'] = $result;
				$place['Place']['photos'] = json_encode($result['photos']);
				if (!$this->Place->save($place)) {
					echo json_encode(['code' => 500, 'msg' => 'Failed to retrieve data']);
					return;
				}


				echo json_encode(['code' => 200, 'data' => json_encode($result)]);
				return;
			} else {
				echo json_encode(['code' => 500, 'data'=>'Nothing']);
				return;
			}
		} else {

			echo json_encode(['code' => 200, 'data' => json_encode($data['Place'])]);
			return;
		}


		echo json_encode(['code' => 500, 'data'=>'Nothing']);


	}

	public function getplaces() {
		$this->autoRender = false;

		$data = $this->request->input('json_decode', true);

		
		if (empty($data)) {
			echo json_encode(["code"=>500, "msg"=>"Invalid parameters"]); die;
		} else {
			$request = new HTTPHelper();
			$result = [];

			foreach ($data['data'] as $key => $value) {
				$datas = json_decode($request->get_places($value['latitude'], $value['longitude'], $data['type'], $data['distance']), true);
				//echo json_encode($datas); die;
				if (!empty($datas['results'])) {
					foreach ($datas['results'] as $k => $val) {
						$photo = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png';
						if (!isset($val['rating']) || $val['rating'] < 4) continue;
						if (isset($val['photos']) && !empty($val['photos'])) {
							$photo =  $request->get_photos($val['photos'][0]['photo_reference']);

						}

						$result[$val['id']] = [
							'id' => $val['id'],
							'place_id' => $val['place_id'],
							'title' => $val['name'],
							'coordinate' => [
								'latitude' => $val['geometry']['location']['lat'],
								'longitude' => $val['geometry']['location']['lng']
							],
							'icon' => $val['icon'],
							'image' => $photo,
							'singlephoto' => (!empty($photo))?$photo:"",

							'rating' => $val['rating'],
							'address' => $val['vicinity'],
							'desc' => "Place Rating : " . $val['rating'] . "\n" . $val['vicinity']
						];
					}


				}

			}

			echo json_encode(array_values($result));
		}
	}
}
